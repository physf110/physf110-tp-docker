FROM frolvlad/alpine-glibc

COPY tl-config/ /tl-config/

RUN apk update && apk add wget perl make &&\
    wget -O - http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz | tar xz &&\
    mv /install-tl-* /install-tl && cd /install-tl && ./install-tl -profile /tl-config/texlive.profile &&\
    tlmgr install $(sed 's/#.*//' /tl-config/packages) &&\
    { tlmgr path add 2>/dev/null; true; } &&\
    rm -rf /install-tl /tl-config &&\
    rm -rf /var/cache/apk/*
