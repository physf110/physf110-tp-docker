Image Docker utilisée dans le processus d'intégration continue mise en place
dans le dépôt principal des séances d'exercices.

L'usage d'une image personnalisée a l'avantage d'être de taille minimale pour
nos besoins. D'autres images TeX Live existent, telles que `tianon/latex`, mais
elles consistent le plus souvent en une installation complète de TeX Live, basée
qui plus est sur une base Debian ou Ubuntu.

Notre image est basée sur `frolvlad/alpine-glibc`, une image Alpine Linux
supplémentée de glibc. Ceci permet l'installation directe des binaires
précompilées de TeX Live, sans souci de compilation ni de compatibilité avec
musl.

L'installation TeX Live est minimale: on reprend l'essentiel de LaTeX avec les
collections `basic` et `latex`, et on ajoute une sélection personnalisée de
paquets par dessus, listés dans le fichier
[tl-config/packages](tl-config/packages).

Le résultat est une installation minimale de TeX Live adaptée à nos besoins.
Pour comparaison, elle fait moins d'un trentième de la taille de `tianon/latex`.
La durée du processus d'intégration continue en est grandement diminuée.
