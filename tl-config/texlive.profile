selected_scheme scheme-basic

TEXDIR /usr/local/texlive/2016
TEXMFCONFIG ~/.texlive2016/texmf-config
TEXMFHOME ~/texmf
TEXMFLOCAL /usr/local/texlive/texmf-local
TEXMFSYSCONFIG /usr/local/texlive/2016/texmf-config
TEXMFSYSVAR /usr/local/texlive/2016/texmf-var
TEXMFVAR ~/.texlive2016/texmf-var

binary_x86_64-linux 1

collection-basic 1
collection-latex 1

option_doc 0
option_src 0
option_fmt 1

option_path 1
option_sys_bin /usr/local/bin
option_sys_info /usr/local/info
option_sys_man /usr/local/man
